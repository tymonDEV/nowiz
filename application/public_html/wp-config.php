<?php

include dirname(__FILE__).'/../configs/wordpress.php';

define('WP_CONTENT_DIR', dirname(__FILE__).'/wp-content');
define('WP_CONTENT_URL', 'http://'.$_SERVER['HTTP_HOST'].'/wp-content');

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('FS_METHOD','direct');

define('AUTOMATIC_UPDATER_DISABLED', false);

define( 'WPCF7_VALIDATE_CONFIGURATION', false );

$table_prefix = 'wp_';

if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__).'/wp/');
}

require_once ABSPATH.'wp-settings.php';
