<?php
$context = Timber::get_context();
$context['page'] = new TimberPost();

Timber::render('views/404/template.twig', $context);
