<?php

$context = Timber::get_context();
$context['post'] = Timber::get_post();

Timber::render('views/single-blog/template.twig', $context);
