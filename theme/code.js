const $ = require('jquery/dist/jquery.min.js');
let Swiper = require('swiper');
let header = require('./components/header/code.js');
let WebFont = require('webfontloader');
document.addEventListener('DOMContentLoaded', function() {

    WebFont.load({
        google: {
          families: ['Lato:100,100i,300,300i,400,400i,700,700i:latin-ext', 'Oswald:200,300,400,500,700:latin-ext']
        }
    });

    let mobile = document.body.classList.contains('mobile');

    header();

    if(mobile) {
        return;
    }
});
