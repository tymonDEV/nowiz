<?php

require_once ABSPATH . '../../vendor/autoload.php';
require_once get_stylesheet_directory()."/components/base/config.php";

// Components
$components = array(
    'base',
    'cocpit'
);


function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'woocommerce_support' );

foreach ($components as $component) {
    require_once get_stylesheet_directory()."/components/$component/backend.php";
}

add_action( 'init', 'ea_acf_options_page' );
/**
 * ACF Options Page for Woocommerce
 * By TymonDev no copy my idea copyright 2017
 */
function ea_acf_options_page() {
    if ( function_exists( 'acf_add_options_sub_page' ) ) {
         acf_add_options_sub_page( array(
            'page_title' => 'CPT Settings',
            'parent'     => 'edit.php?post_type=product',
            'capability' => 'manage_options'
        ) );
    }
}

function timber_set_product( $post ) {
    global $product;

    if ( is_woocommerce() ) {
        $product = wc_get_product( $post->ID );
    }
}