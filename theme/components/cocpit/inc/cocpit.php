<?php

    class Cocpit {

        private static $selectors;
        private static $pages_to_remove;
        private $remove_top_menu = false;
        private $remove_dashboard = false;
        private $modify_login = false;
        private $remove_update_msg = false;
        private $remove_user_table_columns = false;


        /* Notifications
           ========================================================================== */

        public function remove_update_msg_hook() {
            global $wp_version;
            return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
        }

        public function remove_update_msg() {
            $this->remove_update_msg = true;
        }

        /* Top Menu
           ========================================================================== */

        public function remove_top_menu_hook() {

           global $wp_admin_bar;

           $wp_admin_bar->remove_menu('wp-logo');
           $wp_admin_bar->remove_menu('updates');
           $wp_admin_bar->remove_menu('comments');
           $wp_admin_bar->remove_menu('wpseo-menu');
        //    $wp_admin_bar->remove_menu('new-post');
        //    $wp_admin_bar->remove_menu('new-media');
        //    $wp_admin_bar->remove_menu('new-page');

           $wp_admin_bar->remove_node('edit-profile');
        }

        public function remove_top_menu() {
            $this->remove_top_menu = true;
        }

        /* DahsBoard
           ========================================================================== */

        public function remove_dashboard_hook() {
            global $wp_meta_boxes;

            $wp_meta_boxes['dashboard']['normal']['core'] = array();
            $wp_meta_boxes['dashboard']['side']['core'] = array();
        }

        public function remove_dashboards() {
            $this->remove_dashboard = true;
        }

        /* Login Page
           ========================================================================== */

        public function login_page_modify_hook() {
            $logo_url = get_template_directory_uri().'/assets/img/logo.png';
            $bg = '#fff';
            $bg_form = '#000';
            $first_color = '#fff';
            $second_color = '#000';
            $logo_height = '44px';
            $logo_width = '163px';
            $margin_logo = '9rem';

            echo "<style>

                html, body{
                    background: {$bg}!important;
                }

                .login h1 a {display: none}

                #login{
                    padding: 15px 0 0 0;
                }

                .login form {
                    background: {$bg_form};
                    box-shadow: 0 3px 16px rgba(0, 0, 0, 0.1)!important;
                }

                .login form #wp-submit {
                    box-shadow: none;
                    background: transparent;
                    border: 1px solid {$first_color};
                    color: #fff;
                    text-shadow: none;
                    border-radius: 0;
                    padding: 0 21px;
                    margin-top: 30px;
                    transition: 0.3s ease-in-out;
                    height: 30px;
                    line-height: 28px;
                }

                .login form #wp-submit:hover {
                    background: {$first_color};
                    color: #000;
                }

                .login form .input {
                    background: #fff !important;
                    box-shadow: none;
                    border-color: #777;
                    padding: 10px;
                    font-size: 12px;
                }

                .login form input:focus {
                    border-color: {$first_color}; !important
                    box-shadow: none; !important;
                    text-shodow: none; !important
                }

                .login form label {
                    color: #fff;
                    font-size: 11px;
                }

                .login #login_error, .login .message {
                    border-left : none;
                    background: {$first_color};
                    color: #fff;
                    margin: 10px 0;
                }

                .login #login_error a {
                    color: #fff !important;
                }

                .login h1{
                    background: url({$logo_url}) no-repeat;
                    height: {$logo_height};
                    width: {$logo_width};
                    background-size: contain;
                    margin-top: {$margin_logo};
                    padding: 0;
                    text-indent: -9999px;
                    outline: 0;
                    overflow: hidden;
                    }

                 .forgetmenot {
                    display: none;
                 }

                 #nav,
                 #nav a,
                 #backtoblog a{
                    display: none;
                    color: #fff !important;
                }

            </style>";

        }

        /* Remove Menu Pages
           ========================================================================== */

        public function remove_menu_page($page) {
            self::$pages_to_remove[] = $page;
        }

        /* Selectors
           ========================================================================== */

        public function hide_elements_by_selectors() {
            $electors= implode(',', self::$selectors );
            echo "<style> $electors {display: none!important;} </style>";
        }

        public function add_to_hide($selector) {
            self::$selectors[] = $selector;
        }

        /* Edit user profile
           ========================================================================== */

        public function remove_boxes_profile_page(){
            remove_action( 'edit_user_profile', array( 'WPSEO_Admin_User_Profile', 'user_profile' ) );
        }

        public function styles_models_table_page() {
            $screen = get_current_screen();
            if($screen->post_type !== 'model') {
                return;
            }
        ?>
              <style>
                .manage-column.column-0 {
                    width: 120px;
                }
              </style>
        <?php
        }

        /**
         * Call after adding all setings to plugins
         * @return void
         */
        public function init() {

            /* Top Menus
               ========================================================================== */

            if( $this->remove_top_menu ) {
                add_action( 'wp_before_admin_bar_render', array($this, 'remove_top_menu_hook' ) ) ;
            }

            /* Dasboards
               ======================================================`=================== */

            if( $this->remove_dashboard ) {
                add_action( 'wp_dashboard_setup', array($this, 'remove_dashboard_hook' ), 999, 0 ) ;
            }

            /* Login Page
               ========================================================================== */

            if( $this->modify_login ) {
                add_action('login_head', array($this, 'login_page_modify_hook'));
            }

            /* Elements
               ========================================================================== */

            add_action('admin_head', array($this, 'hide_elements_by_selectors'));

            /* Menu
               ========================================================================== */

            if($this->remove_update_msg) {
                add_filter('pre_site_transient_update_core', array($this, 'remove_update_msg_hook') );
                add_filter('pre_site_transient_update_plugins', array($this, 'remove_update_msg_hook'));
                add_filter('pre_site_transient_update_themes', array($this, 'remove_update_msg_hook'));
            }

            /* Remove update notifications
               ========================================================================== */

            add_action( 'admin_head', array($this, 'hide_update_notice'), 1 );

            /* Add styles
               ========================================================================== */

            add_action( 'admin_enqueue_scripts', array($this, 'add_styles'), 20 );
        }

        public function add_styles() {
            global $wp_scripts;
        }

        public function change_options_acf_page($settings){
            $settings['title'] = 'Opcje';
            $settings['menu'] = 'Opcje';
            return $settings;
        }

        function hide_update_notice() {
             remove_action( 'admin_notices', 'update_nag', 3 );
        }

        public function remove_wp_labels() {
            $this->add_to_hide('#footer-thankyou');
            $this->add_to_hide('#footer-upgrade');
        }

        public function clean_user_table() {
        }

        public function remove_user_columns_filter($columns) {
            unset($columns['username']);
            unset($columns['name']);
            // unset($columns['role']);
            unset($columns['posts']);
            return $columns;
        }

        public function hide_elements_from_cocpit(){

            $this->modify_login = true;
            $this->remove_dashboards();
            $this->remove_wp_labels();

            $this->remove_top_menu();
            $this->remove_update_msg();

            if(current_user_can('administrator')) return;

            /* Edit page panel
               ========================================================================== */

            // $this->add_to_hide('#screen-options-wrap');
            // $this->add_to_hide('#screen-options-link-wrap');

            /* Help panel
               ========================================================================== */

            // $this->add_to_hide('#contextual-help-link');
            // $this->add_to_hide('#contextual-help-wrap');

            /* Left menu
               ========================================================================== */

            // $this->remove_menu_page('profile.php');
            // $this->remove_menu_page('edit-comments.php');
            // $this->remove_menu_page('options-general.php');
            $this->remove_menu_page('edit.php');
            // $this->remove_menu_page('plugins.php');
            // $this->remove_menu_page('edit-comments.php');
            // $this->remove_menu_page('customize.php');
            // $this->add_to_hide('#menu-settings');
            // $this->add_to_hide('#menu-comments');
            // $this->add_to_hide('#menu-plugins');
            // $this->add_to_hide('#menu-posts-model');
            // $this->add_to_hide('#menu-tools');
            // $this->add_to_hide('#menu-posts-presentation');
            $this->add_to_hide('.toplevel_page_wpcf7');
            // $this->add_to_hide('#toplevel_page_sent_message');
            // $this->add_to_hide('#toplevel_page_wpseo_dashboard');

            // Model Site

            /* Polylang
            ========================================================================== */
            //
            // $this->add_to_hide('.settings_page_mlang .nav-tab-wrapper');
            // $this->add_to_hide('.settings_page_mlang .column-name');

            // SEO
            // $this->add_to_hide('.submitdelete');

            // ACF
            // $this->add_to_hide('#toplevel_page_edit-post_type-acf-field-group');
            // $this->add_to_hide('#footer-left');
        }

    }
