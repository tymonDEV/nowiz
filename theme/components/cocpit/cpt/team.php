<?php

function team_post_type() {

    $labels = array(
        'name'                => __( 'Team members', 'text-domain' ),
        'singular_name'       => __( 'Team members', 'text-domain' ),
        'menu_name'           => __( 'Team members', 'text-domain' ),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => '',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => array('slug' => 'team'),
        'capability_type'     => 'post',
        'menu_icon'           => 'dashicons-groups',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail' )
    );

    register_post_type( 'Team', $args );
}

add_action( 'init', 'team_post_type');
