<?php

function works_post_type() {

    $labels = array(
        'name'                => __( 'Our Best Works', 'text-domain' ),
        'singular_name'       => __( 'Our Best Works', 'text-domain' ),
        'menu_name'           => __( 'Our Best Works', 'text-domain' ),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => '',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-portfolio',
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => array('slug' => 'works'),
        'capability_type'     => 'post'
    );

    register_post_type( 'Works', $args );
}

add_action( 'init', 'works_post_type');
