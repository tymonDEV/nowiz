<?php
add_filter( 'wpcf7_validate_configuration', '__return_false' );

add_theme_support( 'post-thumbnails' );

/* Remove all wordpress scripts and styles
   ========================================================================== */
function enqueue_scripts(){
    global $wp_scripts, $wp_styles;

    $not_remove = array('admin-bar','yoast-seo-adminbar', 'contact-form-7');

    foreach( $wp_scripts->queue as $handle ) :
        if(!in_array($handle, $not_remove)) wp_deregister_script($handle);
    endforeach;

    foreach( $wp_styles->queue as $handle ) :
        if(!in_array($handle, $not_remove))  wp_deregister_style($handle);
    endforeach;
}
add_action('wp_enqueue_scripts','enqueue_scripts');

function deregister_scripts_embed(){
  wp_deregister_script( 'wp-embed' );
}

function disable_wp_emojicons() {
   remove_action( 'admin_print_styles', 'print_emoji_styles' );
   remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
   remove_action('wp_head', 'wp_oembed_add_host_js');
   remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
   remove_action( 'wp_print_styles', 'print_emoji_styles' );
   remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
   remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
   remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action('init', 'disable_wp_emojicons', 9999);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');

function theme_militu_init(){
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    register_nav_menu('sitemap', 'Sitemap');
    acf_add_options_page();
}
add_action('init', 'theme_militu_init');

function theme_militu_add_global_info_to_context($context){
  $context['menu'] = array(
      'sitemap' => new TimberMenu('sitemap')
    );
    $context['ajax'] = admin_url('admin-ajax.php');
    $context['options'] = get_fields('options');
    return $context;
}
add_filter('timber_context', 'theme_militu_add_global_info_to_context');

function template_add_mobile_class_to_body($classes) {
    $classes[] = wp_is_mobile() ? 'mobile' : 'screen';
    $classes[] = is_front_page() ? 'front-page' : '';
    return $classes;
}
add_filter('body_class', 'template_add_mobile_class_to_body');

function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}

add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );

add_action('admin_head', 'my_custom_css');

function my_custom_css() {
  echo '<style>
    .acf-table .acf-row:nth-child(2n+1) td {
        background: rgba(243, 243, 243, 0.73);
    }
  </style>';
}
