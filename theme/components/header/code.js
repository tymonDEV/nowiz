module.exports = function() {
    let body = document.querySelector('html, body');
    let header = document.querySelector('.header'),
    didScroll = false;

    function scrollTrue() {
        didScroll = true;
    }

    function isOnViewpoint(){
        if(body.scrollTop > 100) {
            header.classList.add('hightlight');
        } else {
            header.classList.remove('hightlight');
        }
    }

    document.addEventListener('scroll', function(){
        scrollTrue();
    });

    setInterval(function() {
        if(didScroll) {
            didScroll = false;
            isOnViewpoint();
        }
    }, 301);


};