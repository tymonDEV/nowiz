var del = require('del');
var gulp = require('gulp');
var path = require('path');
var when = require('gulp-if');
var stylus = require('gulp-stylus');
var sequence = require('gulp-sequence');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer-stylus');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var synchronizer = require('browser-sync').create();
var gutil = require('gutil');
var ftp = require('vinyl-ftp');
var babel = require('gulp-babel');
var gutil = require('gulp-util');

const paths = {
    source: 'theme',
    destination: 'application/public_html/wp-content/themes/',
    stylesheet: 'theme/style.styl',
    stylesheets: 'theme/**/*.styl',
    javascripts : 'theme/**/*.js',
    javascript: 'theme/code.js',
    copyable: 'theme/**/*.{php,twig,eot,ttf,woff,woff2,ico,svg,png,jpeg,jpg,webm,mp4,gif,json}',
    vendors: {
        src: ['node_modules/webfontloader/webfontloader.js', 'node_modules/swiper/dist/js/swiper.min.js'],
        desintation: 'application/public_html/wp-content/themes/theme/vendors'
    }
};

paths.destination = paths.destination + paths.source;

const config = {
    host: 'nowiz.dev',
    stage: {
        dest : '/public_html/wp-content/themes/' + paths.source
    },
    production: {}
};

var relative = {
    base: paths.source
};

function stylesheets(development) {
    return function() {
        return gulp.src(paths.stylesheet, relative)
            .pipe(when(development, sourcemaps.init()))
            .pipe(stylus({
                'use': [autoprefixer({ browsers: ['Firefox >= 28','Chrome >= 21','Safari >= 6.1','Opera >= 12.1','Explorer >= 10','iOS >= 7.1','Android >= 4.4','OperaMobile >= 12.1','ChromeAndroid >= 44','FirefoxAndroid >= 40','ExplorerMobile >= 10'] })],
                'include css': true,
                'compress': true,
                'include': [path.join(__dirname, 'node_modules')]
            }))
            .pipe(when(development, sourcemaps.write()))
            .pipe(when(!development, cssmin()))
            .pipe(gulp.dest(paths.destination))
            .pipe(when(development, synchronizer.stream()));
    };
}

function javascripts(development) {
    return function() {
        return gulp.src(paths.javascript, relative)
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(browserify({
                insertGlobals: true,
                debug: development
            }))
            .pipe(gulp.dest(paths.destination))
            .pipe(when(development, synchronizer.stream()));
    };
}

function deploy(type) {
    var conn = ftp.create(config[type]);

    return function() {
        return gulp.src(paths.destination + '/**/*', {base : paths.destination, buffer: false})
            .pipe(conn.newer(config[type].dest)) // only upload newer files
            .pipe(conn.dest(config[type].dest));
    };
}

gulp.task('javascripts-develop', javascripts(true));
gulp.task('javascripts-production', javascripts(false));
gulp.task('stylesheets-develop', stylesheets(true));
gulp.task('stylesheets-production', stylesheets(false));
gulp.task('deploy-stage', deploy('stage'));
gulp.task('deploy-production', deploy('production'));

gulp.task('clean', function() {
    return del(paths.destination);
});

gulp.task('copy', function() {
    return gulp.src(paths.copyable, relative)
        .pipe(gulp.dest(paths.destination))
        .pipe(synchronizer.stream());
});

gulp.task('vendors', function() {
    return gulp.src(paths.vendors.src)
        .pipe(gulp.dest(paths.vendors.desintation))
        .pipe(synchronizer.stream());
});

gulp.task('synchronize', function() {
    synchronizer.init({
        proxy: config.host
    });
});

gulp.task('build-develop', sequence('clean', 'synchronize', 'vendors', 'copy', 'stylesheets-develop', 'javascripts-develop'));

gulp.task('develop', ['build-develop'], function() {
    gulp.watch(paths.copyable, ['copy']);
    gulp.watch(paths.javascript, ['javascripts-develop']);
    gulp.watch(paths.stylesheets, ['stylesheets-develop']);
    gulp.watch(paths.javascripts, ['javascripts-develop']);
});

gulp.task('build', sequence('clean', 'copy', 'vendors' ,'stylesheets-production', 'javascripts-production'));
